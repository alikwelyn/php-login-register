<?php require_once 'includes/header.php'; ?>

<div class="container">
  <div class="row">
    <div class="col-md-12 mt-3 text-center">
      <h1>REGISTER</h1>
      <p>Alredy have an account ? <a href="register.php">Login here!</a></p>
    </div>
    <div class="col-md-12 mt-3">
        <form action="includes/register-inc.php" method="POST">
            <div class="form-group">
                <label for="Username">Username</label>
                <input type="text" class="form-control" name="username" placeholder="Username">
            </div>
            <div class="form-group">
                <label for="Password">Password</label>
                <input type="password" class="form-control" name="password" placeholder="Password">
            </div>
            <div class="form-group">
                <label for="Password">Confirm Password</label>
                <input type="password" class="form-control" name="confirmPassword" placeholder="Confirm Password">
            </div>
            <button type="submit" class="btn btn-primary" name="submit">Submit</button>
        </form>
    </div>
  </div>
</div>

<?php require_once 'includes/footer.php'; ?>