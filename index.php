<?php require_once 'includes/header.php'; ?>

<div class="container">
  <div class="row">
    <div class="col-md-12 text-center mt-3">
      <h1>HOME</h1>
    </div>
    <div class="col-md-12 mt-3">
        <?php 
        
        if (isset($_SESSION['sessionID'])) {
          echo "You are logged in!";
        } else {
          echo "Plis, login to see the content";
        }

        ?>
    </div>
  </div>
</div>

<?php require_once 'includes/footer.php'; ?>